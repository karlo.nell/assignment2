﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabasSQL.Models
{
    public class CustomerSpender
    {
        public int CustomerId { get;  set; }
        public decimal TotalSpending { get; set; }
    }
}
