﻿using DatabasSQL.Data_Access.Repositories;
using DatabasSQL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabasSQL.Controller
{
    public class CustomerController 
    {
        private readonly ICustomerRepository _db;
        public CustomerController(ICustomerRepository db)
        {
            _db = db;
        }
        /// <summary>
        /// Gets the Customer from database matching the ID provided.
        /// </summary>
        /// <param name="InputCustomerID">The customer ID used to query the database to find the Customer.</param>
        /// <returns>Found Customer. If not found returns Console Error message.</returns>
        public Customer GetCustomerByID(int CustomerId)
        {
            return _db.GetCustomerById(CustomerId);
        }
        /// <summary>
        /// Gets all customers in database.
        /// </summary>
        /// <returns>An Enumerable list of customers. If no customers exist writes Console error message. If failed throws SqlException and shows error message. </returns>
        public IEnumerable<Customer> GetAllCustomers()
        {
            return _db.GetAllCustomers();
        }
        /// <summary>
        /// Gets customers matching their First Name to searchString (case insensitive).
        /// </summary>
        /// <param name="searchString">The search parameter for FirstName.</param>
        /// <returns>An Enumerable list of customers matching the searchString in their First Name. If failed throws SqlException and shows error message.</returns>
        public IEnumerable<Customer> GetCustomerByFirstName(string CustomerName)
        {
            return _db.GetCustomerByFirstName(CustomerName);
        }

        /// <summary>
        /// Gets all customers and selects based on specified OFFSET and FETCH NEXT values. 
        /// </summary>
        /// <param name="offset">Specifies the number of rows to skip before starting to return rows from the query.</param>
        /// <param name="fetch">Specifies the number of rows to return after the OFFSET clause has been processed.</param>
        /// <returns>An Enumerable list of customers. If failed throws SqlException and shows error message.</returns>
        public IEnumerable<Customer> GetCustomersByOffsetAndFetch(int Offset, int fetch)
        {
            return _db.GetCustomersByOffsetAndFetch(Offset, fetch);
        }
        /// <summary>
        /// Inserts the provided Customer object into the database. Please ignore declaring CustomerID as its AutoIncremented. Check Parameter for values supported.
        /// </summary>
        /// <param name="customer">Leave CustomerId undeclared as its AutoIncremented. Values supported: FirstName, LastName, Country, PostalCode, Phone, Email</param>
        /// <returns>True if affected rows are equal to 1. If failed throws SqlException and shows error message.</returns>
        public bool InsertCustomer(Customer customer)
        {
            return _db.InsertCustomer(customer);
        }
        /// <summary>
        /// Updates the Customer object in the database matching the Customer property CustomerID. Updates only non null fields. CustomerId required. Check parameter for values supported.
        /// </summary>
        /// <param name="customer">The Customer with CustomerId declared and declared fields to update in database. 
        /// Values supported: FirstName, LastName, Country, PostalCode, Phone, Email</param>
        /// <returns>True if affected rows are larger or equal to 1. If failed throws SqlException and shows error message.</returns>
        public bool UpdateCustomer(Customer customer)
        {
            return _db.UpdateCustomer(customer);
        }
        /// <summary>
        /// Gets each country's customer count.
        /// </summary>
        /// <returns>An Enumerable list of CustomerCountry. If no customers with country present returns Console error message.
        /// If failed throws SqlException and shows error message</returns>
        public IEnumerable<CustomerCountry> GetCustomerCountInCountries()
        {
            return _db.GetCustomerCountInCountries();
        }
        /// <summary>
        /// Gets all customers grouped by total spent generated by invoices.
        /// </summary>
        /// <returns>An Enumerable list of CustomerSpender.  If no customer matches returns Console error message.
        /// If failed throws SqlException and shows error message</returns>
        public IEnumerable<CustomerSpender> GetCustomersByTotalSpent()
        {
            return _db.GetCustomersByTotalSpent();
        }
        /// <summary>
        /// Gets the Customer's most popular genre corresponding to most tracks from invoices associated to the customer. 
        /// In case of a tie displays both genre's.
        /// </summary>
        /// <param name="customerId">The customer ID used to query the database.</param>
        /// <returns>An Enumerable list of CustomerGenre corresponding to most track from invocies 
        /// matching the searchString in their First Name. If no customer matches returns Console error message.
        ///If failed throws SqlException and shows error message</returns>
        public IEnumerable<CustomerGenre> GetCustomerMostPopularGenre(int customerId)
        {
            return _db.GetCustomerMostPopularGenre(customerId);
        }

    }
}
