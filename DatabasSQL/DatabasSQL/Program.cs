﻿using DatabasSQL.Controller;
using DatabasSQL.Data_Access.Repositories;
using DatabasSQL.DataAccess;
using DatabasSQL.Models;
using System;

namespace DatabasSQL
{
    class Program
    {
        /// <summary>
        /// Uncomment parts below to run according to Customer Requirements in Assignemnt 2 Appendix B.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //Assigment 2 -- Appendix B - Reading Data with SQLClient

           
            //Used as a business class for repository (Dont remove :) )
            CustomerController CC = new CustomerController(new CustomerRepository());
            

            //-----1.-----Read all customers in db---------
            //Console.WriteLine("\t\t\t************* Getting all customers *************");
            //CC.GetAllCustomers();

            //-----2.-----Read a specific customer matching the ID---------
            //Console.WriteLine("\n\t\t\t************* Getting customer with ID *************");
            //CC.GetCustomerByID(4);

            //-----3.----- Read a specific customer by name (although this gives several matches if not uniquely specified searchstring)-------
            //Console.WriteLine("\n\t\t\t************* Getting customers matching first name search query *************");
            ////Case insensitive search.
            //CC.GetCustomerByFirstName("Aaron");

            //-----4.-----Return a page of customers from the database with offset and fetch next (LIMIT) parameters ---------
            //Console.WriteLine("\n\t\t\t************* Getting customers with provided offset and limit(fetch) values *************");
            //CC.GetCustomersByOffsetAndFetch(2,10);


            //-----5.-----Adds a new customer to the database---------
            //INSERT CUSTOMER
            //Customer cm = new Customer()
            //{
            //    FirstName = "John",
            //    LastName = "Wick",
            //    Country = "USA",
            //    PostalCode = "52339",
            //    Phone = "+001231832832",
            //    Email = "john.wick.usa@murica.com"
            //};
            //Console.WriteLine("Inserted customer: {0}", CC.InsertCustomer(cm));

            //-----6.-----UPDATE Existing Customer.----------
            //Customer cm1 = new Customer()
            //{
            //    CustomerId = 61,
            //    FirstName = "John", 
            //    LastName = "Wick",
            //    Email = "johnnywick@msn.se",
            //    Phone = "12345",
            //    PostalCode = "12345",
            //    Country = "Sverige"
            //};
            //CC.UpdateCustomer(cm1);

            //-----7.-----Customers in each country ordered descending.------
            //CC.GetCustomerCountInCountries();

            //-----8.-----Customers who are highest spenders ordered descending.-------
            //CC.GetCustomersByTotalSpent();

            //-----9.-----Customers by most popular genre. Displays all if has two or more genres.----
            //for (int i = 1; i < 60 + 1; i++)
            //{

            //    CC.GetCustomerMostPopularGenre(100);
            //}

        }
    }
}
